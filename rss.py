#       Redistribution and use in source and binary forms, with or without
#       modification, are permitted provided that the following conditions are
#       met:
#       
#       * Redistributions of source code must retain the above copyright
#         notice, this list of conditions and the following disclaimer.
#       * Redistributions in binary form must reproduce the above
#         copyright notice, this list of conditions and the following disclaimer
#         in the documentation and/or other materials provided with the
#         distribution.
#       * Neither the name of the  nor the names of its
#         contributors may be used to endorse or promote products derived from
#         this software without specific prior written permission.
#       
#       THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#       "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#       LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#       A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#       OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#       SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#       LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#       DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#       THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#       (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#       OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#	Some of the code (especially the template rendering and css) 
#	was take from the book:
#	Using Google App Engine by Charles Severance
#	Copyright May 2009 ISBN:978-0-596-80069-7


import wsgiref.handlers
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext import db
import os
from main import Patch


def doRender(handler, tname = 'rss.xml', values = { }):
  temp = os.path.join(
      os.path.dirname(__file__),tname)

  # Make a copy of the dictionary and add the path and session
  newval = dict(values)
  newval['path'] = handler.request.path

  outstr = template.render(temp, newval)
  handler.response.out.write(outstr)
  return True
  



class RssHandler(webapp.RequestHandler):
	def get(self,bsd_version):
		#print bsd_version
		patches=Patch.all().order("-bsd_version").order("-patch_title")
		if bsd_version!='all':
			patches.filter('bsd_version = ',bsd_version)
		doRender(self,'rss.xml',{'patches':patches,'bsd_version':bsd_version})
		

def main():
  application = webapp.WSGIApplication([
	('/feed/([^/]+)', RssHandler)],
     debug=True)
  wsgiref.handlers.CGIHandler().run(application)
  
if __name__ == '__main__':
	main()
