#       Redistribution and use in source and binary forms, with or without
#       modification, are permitted provided that the following conditions are
#       met:
#       
#       * Redistributions of source code must retain the above copyright
#         notice, this list of conditions and the following disclaimer.
#       * Redistributions in binary form must reproduce the above
#         copyright notice, this list of conditions and the following disclaimer
#         in the documentation and/or other materials provided with the
#         distribution.
#       * Neither the name of the  nor the names of its
#         contributors may be used to endorse or promote products derived from
#         this software without specific prior written permission.
#       
#       THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#       "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#       LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#       A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#       OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#       SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#       LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#       DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#       THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#       (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#       OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#	Some of the code (especially the template rendering and css) 
#	was take from the book:
#	Using Google App Engine by Charles Severance
#	Copyright May 2009 ISBN:978-0-596-80069-7


import wsgiref.handlers
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext import db
import os
import datetime
import urllib2
import re
import string
import hashlib
import logging
from BeautifulSoup import BeautifulSoup
from HTMLParser import HTMLParser


months={ 
                    'January':1, 
                    'February':2, 
                    'March':3, 
                    'April':4, 
                    'May':5, 
                    'June':6, 
                    'July':7, 
                    'August':8, 
                    'September':9, 
                    'October':10, 
                    'November':11, 
                    'December':12, 
                    'Jan':1, 
                    'Feb':2, 
                    'Mar':3, 
                    'Apr':4, 
                    'May':5, 
                    'Jun':6, 
                    'Jul':7, 
                    'Aug':8, 
                    'Sep':9, 
                    'Oct':10, 
                    'Nov':11, 
                    'Dec':12, 
            }

class Feed(db.Model):
	bsd_version=db.StringProperty()
	url=db.StringProperty()
	date_added=db.DateTimeProperty(auto_now_add=True)

class Patch(db.Model):
	bsd_version=db.StringProperty()
	date=db.DateTimeProperty()
	patch_uid=db.StringProperty()
	patch_title=db.StringProperty()
	date_added=db.DateTimeProperty(auto_now_add=True)
	patch_text=db.TextProperty()
	patch_link=db.TextProperty()

def doRender(handler, tname = 'index.html', values = { }):
  temp = os.path.join(
      os.path.dirname(__file__),tname)

  # Make a copy of the dictionary and add the path and session
  newval = dict(values)
  newval['path'] = handler.request.path

  outstr = template.render(temp, newval)
  handler.response.out.write(outstr)
  return True
  

class GenFeedHandler(webapp.RequestHandler):
	def get(self):
		
		feeds=Feed().all()
		for feed in feeds:
			#print feed.url
			#try:
			get_patches(feed.url,feed.bsd_version)
			#except : pass
			print "Ok <br />"

class TestHandler(webapp.RequestHandler):
	def get(self):
		feeds=Feed.all().order("-bsd_version")
		patches=Patch.all().order("-patch_title")
		doRender(self,'about.html',{'feeds':feeds, 'patches':patches})
		
class AddHandler(webapp.RequestHandler):
	def get(self):
		doRender(self,'add.html')
	def post(self):
		new_feed=Feed()
		new_feed.bsd_version=self.request.get('version')
		new_feed.url=self.request.get('url')
		new_feed.put()
		self.redirect('/generatefeed')
		self.response.out.write(
            '<!doctype html><html><body><a href="/generatefeed">generatefeed</a></body></html>')

class MLStripper(HTMLParser): 
    def __init__(self):         
        self.reset()         
        self.fed = []     
    def handle_data(self, d): 
        self.fed.append(d)     
    def get_data(self):         
        return ''.join(self.fed) 

def strip_tags(html):     
    s = MLStripper()     
    s.feed(html)    
    return s.get_data() 		

def get_patches(url,bsd_version):
	
	page=BeautifulSoup(urllib2.urlopen(url))
	#build a list of exixting patches
	existing_patch_list=[]
	existing_patches=Patch.all().filter('bsd_version = ',bsd_version)
	for existing_patch in existing_patches:
		existing_patch_list.append(existing_patch.patch_uid)
		
	

	for i in range(len(page('li'))):
		# try to find the link to the patch
		for link_number in range(len(page('li')[i].findAll('a'))):
				link_href=""
				try:
					link_href = page('li')[i].findAll('a')[link_number]['href']
				except:
					pass
				if "OpenBSD/patches" in link_href:
					patch_link=link_href
				else:
					patch_link=url
		#clean up the list element and parse out the date
		try:
			desc = strip_tags(str(page('li')[i]))
			#print desc,"<p>"
			feed_title=desc.split(':')[0]+ ' ' + desc.split(':')[1]
		except:pass
		try:
			feed_date_M_D=desc.split(':')[2].split(',')[0]
			feed_date_Y=desc.split(':')[2].split(',')[1].split(' ')[1]
		
			try:
				feed_date_datetime=datetime.datetime(int(feed_date_Y),
											months[feed_date_M_D.split()[0]],
											int(feed_date_M_D.split()[1])
											)
			except:
				feed_date_datetime=datetime.date(1900,1,1)
			#print feed_date_datetime
			#print feed_date_M_D,feed_date_Y
			#print string.strip(feed_title)
			#print string.strip(desc) 
			
			title_hash=hashlib.md5(feed_title+bsd_version)
			
			patch=Patch()
			patch.bsd_version=bsd_version
			patch.date=feed_date_datetime
			patch.patch_uid=title_hash.hexdigest()
			patch.patch_title=string.strip(feed_title)
			patch.patch_text=string.strip(desc)
			patch.patch_link=patch_link
			
			#don't add an exixting patch
			if patch.patch_uid not in existing_patch_list:
				
				patch.put()
		except:pass
		

def main():
  application = webapp.WSGIApplication([
   	('/about.html', TestHandler),
   	('/add.html', AddHandler),
   	('/generatefeed', GenFeedHandler),
	('/.*', TestHandler)],
     debug=True)
  wsgiref.handlers.CGIHandler().run(application)
  
if __name__ == '__main__':
	main()
